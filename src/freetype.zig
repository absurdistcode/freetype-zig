const std = @import("std");
const c = @cImport({
    @cInclude("freetype/freetype.h");
});

const FT_Error = enum(c_int) {
    success = 0,
    _,
};

const Library = opaque {
    pub fn init() !*Library {
        var ret: *Library = undefined;

        if (FT_Init_FreeType(&ret) != .success) {
            return error.FailedToInitLibrary;
        }

        return ret;
    }

    extern fn FT_Init_FreeType(**Library) FT_Error;

    pub fn deinit(self: *Library) bool {
        return FT_Done_FreeType(self) == .success;
    }

    extern fn FT_Done_FreeType(*Library) FT_Error;

    pub fn createFace(self: *Library, path: [*:0]const u8, face_index: u32) !*Face {
        var ret: *Face = undefined;

        if (FT_New_Face(self, path, @intCast(face_index), *ret) != .success) {
            return error.FailedToCreateFace;
        }

        return ret;
    }

    extern fn FT_New_Face(library: *Library, filepathname: [*:0]const u8, face_index: c_long, face: **Face) FT_Error;

    pub fn createFaceFromMemory(self: *Library, data: []const u8, face_index: u32) !*Face {
        var ret: *Face = undefined;

        const len = std.math.cast(c_long, data.len) orelse return error.dataToLong;

        if (FT_New_Memory_Face(self, data.ptr, len, @intCast(face_index), &ret) != .success) {
            return error.FailedToCreateFace;
        }

        return ret;
    }

    extern fn FT_New_Memory_Face(library: *Library, file_base: [*]const u8, file_size: c_long, face_index: c_long, face: **Face) FT_Error;
};

const Pos = c_long;

const BitmapSize = struct {
    height: c_short,
    width: c_short,

    size: Pos,

    x_ppem: Pos,
    y_ppem: Pos,
};

const Face = opaque {
    pub fn setPixelSizes(self: *Face, pixel_width: u32, pixel_height: u32) !void {
        if (FT_Set_Pixel_Sizes(self, @intCast(pixel_width), @intCast(pixel_height)) != .success) {
            return error.GenericError;
        }
    }

    extern fn FT_Set_Pixel_Sizes(self: *Face, pixel_width: c_uint, pixel_height: c_uint) FT_Error;

    const LoadGlyphFlags = packed struct(i32) {
        no_scale: bool = false,
        no_hinting: bool = false,
        render: bool = false,
        no_bitmap: bool = false,
        vertical_layout: bool = false,
        force_autohint: bool = false,
        crop_bitmap: bool = false,
        pedantic: bool = false,
        ignore_global_advance_width: bool = false,
        no_recurse: bool = false,
        ignore_transform: bool = false,
        monochrome: bool = false,
        linear_design: bool = false,
        no_autohint: bool = false,
    };

    pub fn loadGlyph(self: *Face, glyph_index: u32, flags: LoadGlyphFlags) !void {
        if (FT_Load_Glyph(self, @intCast(glyph_index), @bitCast(flags)) != .success) {
            return error.GenericError;
        }
    }

    extern fn FT_Load_Glyph(self: *Face, glyph_index: c_uint, load_flags: i32) FT_Error;

    pub fn glyph(self: *Face) *Glyph {
        const ftFace = @as(*c.FT_FaceRec_, @ptrCast(@alignCast(self)));

        return @ptrCast(ftFace.glyph);
    }
};

const Glyph = opaque {};

const testing = @import("std").testing;

test "init library" {
    const lib = try Library.init();

    const face = try lib.createFaceFromMemory(&[_]u8{ 1, 2, 3 }, 0);

    const g = face.glyph();
    _ = g;

    try testing.expect(lib.deinit());
}
